<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $nickname
 * @property string $email
 * @property string $password
 * @property string $auth_key
 * @property string $access_token
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nickname', 'email'], 'required'],
            [['nickname', 'email', 'password', 'auth_key', 'access_token'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'nickname' => 'Nickname',
            'email' => 'Email',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) : ?self {
        return self::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) : ?self {
        return self::findOne(['access_token' => $token]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId() : ?int {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername() : ?string {
        return $this->nickname;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() : ?string {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) : bool
    {
        return $this->auth_key === $authKey;
    }

    /**
     * @param string $username
     * @return null|static
     */
    public static function findByUserName($username) {
        return self::findOne(['email' => $username]);
    }

    /**
     * @param string $password
     * @return void
     */
    public function setPassword(string $password) : void {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword(string $password) : bool {
        return Yii::$app->security->validatePassword($password, $this->password);
    }
}
