<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tag_to_post`.
 */
class m180612_180919_create_tag_to_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tag_to_post', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer(),
            'tag_id' =>  $this->integer()
        ]);

        $this->addForeignKey('mediator_to_post', 'tag_to_post', 'post_id', 'post', 'id');
        $this->addForeignKey('mediator_to_tag', 'tag_to_post', 'tag_id', 'tag', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tag_to_post');
    }
}
