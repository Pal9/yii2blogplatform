<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tag`.
 */
class m180612_175459_create_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tag', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->unique(),
            'created_at' => $this->string()
        ]);

        $this->addForeignKey('tag_to_post_mediator', 'tag', 'id', 'post', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tag');
    }
}
