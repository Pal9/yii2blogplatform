<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m180612_173618_create_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'body' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'post' => $this->integer()->notNull(),
            'author' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('comment_to_post', 'comment', 'post', 'post', 'id');
        $this->addForeignKey('comment_to_author', 'comment', 'author', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('comment');
    }
}
