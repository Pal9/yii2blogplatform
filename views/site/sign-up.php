<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.06.2018
 * Time: 20:04
 */
use yii\bootstrap\ActiveForm;

?>

<div class="container">
    <h1>Регистрация пользователя</h1>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($user, 'nickname')->input('string'); ?>
    <?= $form->field($user, 'email')->input('email'); ?>
    <?= $form->field($user, 'password')->passwordInput(); ?>
    <button type="submit" class="btn btn-sm btn-success">Регистрация</button>
    <?php ActiveForm::end(); ?>
</div>
