<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays sign-up page.
     *
     * @return string
     */
    public function actionSignUp() {

        $user = new User();

        if ($user->load(Yii::$app->request->post())) {

            $user->setPassword($user->password);
            $user->save();

            $role = Yii::$app->authManager->getRole('reader');
            Yii::$app->authManager->assign($role, $user->id);

            return $this->redirect(['site/login']);
        }

        return $this->render('sign-up', [
            'user' => $user,
        ]);
    }

    /**
     * Инициация ролей пользователей для однократного запуска на старте проекта после регистрации админской учётки
     */
//    public function actionInitRoles(): void {
//        $adminRole = Yii::$app->authManager->createRole('admin');
//        Yii::$app->authManager->add($adminRole);
//        $clientRole = Yii::$app->authManager->createRole('author');
//        Yii::$app->authManager->add($clientRole);
//        $managerRole = Yii::$app->authManager->createRole('reader');
//        Yii::$app->authManager->add($managerRole);
//
////         привязываем роль админа к пользователю 1:
//        $adminRole = Yii::$app->authManager->getRole('admin');
//
//        $user = User::findOne(1);
//
//        Yii::$app->authManager->assign($adminRole, $user->id);
//    }
}
